var jwtutil = require('../../../util/jwtutil')
var urlutil = require('../../../util/urlutil')

var dbapi = require('../../../db/api')

module.exports = function(options) {
  return function(req, res, next) {
    if (req.query.jwt) {
      // Coming from auth client
      var data = jwtutil.decode(req.query.jwt, options.secret)
      var redir = urlutil.removeParam(req.url, 'jwt')
      if (data) {
        console.log(data.user_name)
        console.log(data.password)
        console.log(data.emailtrue)
        console.log(data.telefono)
        console.log(data.name)
        console.log(data.email)
        

        if(data.password){
          console.log(data.name + "o" +data.user_name)
          dbapi.newUser({

            user_name: data.user_name
          , password: data.password
          , emailtrue: data.emailtrue
          , telefono: data.telefono  
          , ip: req.ip
          })
          .then(function(stats) {
            if(stats.errors=1){
               // Aca se debe colocar el manejo de error, usted ya se encuentra registrado
              //console.log(stats.first_error);
            }
            //req.session.jwt = data
            //res.redirect(redir)
            //res.redirect("/auth/register/success/")
           // $window.open= 'auth/register/success/'
           res.redirect("auth/register/success/")
           //res.redirect("http://172.17.27.145:7100/auth/register/success/")
           /*var app = angular.module('RedirectURLApp', []);
           app.controller('RedirectURLCtrl', function($scope, $window) {
             $scope.name = 'Anil';
             $scope.RedirectToURL = function() {
               var host = $window.location.host;
               var landingUrl = "http://www.google.com";
               alert(landingUrl);
               $window.location.href = landingUrl;
             };
           });*/
          })
          .catch(next)
        }
        else{
          // Redirect once to get rid of the token
        dbapi.saveUserAfterLogin({
          name: data.name
        , email: data.email
        , ip: req.ip
        })
        .then(function(stats) {
          //var name = 'name';
          //var email = 'email';
          
          if (stats.replaced>0)  {
            console.log("Ingreso al if AUTH");
            req.session.jwt = data
            res.redirect(redir)
          //} else if (email==data.email && name!=data.name) {
            //res.redirect("https://www.w3schools.com/js/js_if_else.asp")

          } else {
            console.log("Primer else Jeiner" + options.authUrl);
            //location.replace("https://www.adictosaltrabajo.com/tutoriales/protege-tu-api-rest-mediante-json-web-tokens/")
            //res.status(401).redirect("/Users/pruebasmoviles/stf/res/auth/mock/scripts/signin/Olvido.html")
            res.redirect("/auth/noregister/")
           /* res.status(401).json(
               'No se encuentra autorizado para acceder. Puede acceder a' 
            )  */
            //res.redirect("/#/auth/register")
            //res.redirect("http://localhost:7100/auth/mock/")
            //res.status(401).json({
            //  mensaje: 'No se encuentra autorizado para acceder.'
            //success: false
            //onclick='alert("Bienvenido, ahora ya puedes acceder mediante tu usuario y contraseña")'
            //description: 'Usted no se encuentra registrado, si desea registrarse para comenzar a disfrutar de la granja de móviles ingrese aquí http://www.choucairtesting.com/Contactenos '
         // })
            //res.redirect("http://www.choucairtesting.com/Contactenos")
          }
        })
        .catch(next)
        }
      }
      else {
        // Invalid token, forward to auth client
        console.log("Segundo else " + options.authUrl);
        res.redirect(options.authUrl)
      }
    }
    else if (req.session && req.session.jwt) {
      dbapi.loadUser(req.session.jwt.email)
        .then(function(user) {
          if (user) {
            // Continue existing session
            req.user = user
            console.log("Este es" + req.user.toString);
            next()
          }
          else {
            // We no longer have the user in the database
            res.redirect(options.authUrl)

          }
        })
        .catch(next)
    }
    else {
      // No session, forward to auth client
      res.redirect(options.authUrl)
    }
  }
}
