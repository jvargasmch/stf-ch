var http = require('http')

var express = require('express')
var validator = require('express-validator')
var cookieSession = require('cookie-session')
//var localStorage = require('localStorage')
var bodyParser = require('body-parser')
var serveStatic = require('serve-static')
var csrf = require('csurf')
var Promise = require('bluebird')
var basicAuth = require('basic-auth')

var logger = require('../../util/logger')

var requtil = require('../../util/requtil')
var ldaputil = require('../../util/ldaputil')
var jwtutil = require('../../util/jwtutil')
var pathutil = require('../../util/pathutil')
var urlutil = require('../../util/urlutil')
var lifecycle = require('../../util/lifecycle')

module.exports = function(options) {
  var log = logger.createLogger('auth-mock')
  var app = express()
  var server = Promise.promisifyAll(http.createServer(app))

  lifecycle.observe(function() {
    log.info('Waiting for client connections to end')
    return server.closeAsync()
      .catch(function() {
        // Okay
      })
  })

  // BasicAuth Middleware
  var basicAuthMiddleware = function(req, res, next) {
    function unauthorized(res) {
      res.set('WWW-Authenticate', 'Basic realm=Authorization Required')
      return res.send(401)
    }

    var user = basicAuth(req)

    if (!user || !user.name || !user.pass) {
      return unauthorized(res)
    }

    if (user.name === options.mock.basicAuth.username &&
        user.pass === options.mock.basicAuth.password) {
      return next()
    }
    else {
      return unauthorized(res)
    }
  }

  app.set('view engine', 'pug')
  app.set('views', pathutil.resource('auth/mock/views'))
  app.set('strict routing', true)
  app.set('case sensitive routing', true)

  app.use(cookieSession({
    name: options.ssid
  , keys: [options.secret]
  }))

  /*app.use(localStorage({
    name: options.ssid
  , keys: [options.secret]
  }))*/

  app.use(bodyParser.json())
  app.use(csrf())
  app.use(validator())
  app.use('/static/bower_components',
    serveStatic(pathutil.resource('bower_components')))
  app.use('/static/auth/mock', serveStatic(pathutil.resource('auth/mock')))

  app.use(function(req, res, next) {
    res.cookie('XSRF-TOKEN', req.csrfToken())
    next()
  })

  if (options.mock.useBasicAuth) {
    app.use(basicAuthMiddleware)
  }

  app.get('/', function(req, res) {
    res.redirect('/auth/mock/')
  })

  app.delete('/logout', function(req, res) {
    req.session = null;
    res.redirect('/auth/mock/')
  })

  app.get('/auth/mock/*', function(req, res) {
    res.render('index')
  })

  app.get('/auth/register/', function(req, res){
    res.render('index')
  })
	
  app.get('/auth/register/success/', function(req, res){
    res.render('index')
  })

  app.get('/auth/noregister/', function(req, res){
    res.render('index')
  })


  app.post('/auth/api/v1/mock', function(req, res) {
    var log = logger.createLogger('auth-mock')
    log.setLocalIdentifier(req.ip)

    //var choucair = false;


    //req.checkBody('user_name'.notEmpty && 'email'.notEmpty && 'correo'.notEmpty && 'telefono'.notEmpty)


   // console.log(choucair)
   //var x = document.forms["register"]["telefono"].value;
   //function required(input)

   var telefono = req.body.telefono;
    if (telefono >= 0) {
      console.log("Esto es un register") 
      //var user_name = req.body.user_name;
      var user_name = "Inactivo";
      var email = req.body.email;
      var correo = req.body.correo;
      var telefono = req.body.telefono;
      var name = req.body.name;
      console.log(user_name)
      console.log(email)
      console.log(correo)
      console.log(name) 
      console.log("Esto es un nuevo register") 
      log.info('Authenticated "%s"', req.body.email)
      console.log("Entra al token")
      var token = jwtutil.encode({
        payload: {
          password: req.body.email
        , user_name: req.body.user_name
        , emailtrue: req.body.correo
        , telefono: req.body.telefono
        , email: req.body.email
        , name: req.body.user_name
        }
      , secret: options.secret
      , header: {
          exp: Date.now() + 24 * 3600
        }
      })
      res.status(200)
        .json({
          success: true
          , redirect: urlutil.addParams(options.appUrl, {
            jwt: token
          })
      }) 
    }else{
      console.log("Esto es un login") 
      switch (req.accepts(['json'])) {
        case 'json':
          requtil.validate(req, function() {
              req.checkBody('name').notEmpty()
              console.log(req.checkBody)
              req.checkBody('email').notEmpty()
            })
            .then(function() {
              log.info('Authenticated "%s"', req.body.email)
              var token = jwtutil.encode({
                payload: {
                  email: req.body.email
                , name: req.body.name
                }
              , secret: options.secret
              , header: {
                  exp: Date.now() + 24 * 3600
                }
              })
              res.status(200)
                .json({
                  success: true
                , redirect: urlutil.addParams(options.appUrl, {
                    jwt: token
                  })
                })
            })
            .catch(requtil.ValidationError, function(err) {
              res.status(400)
                .json({
                  success: false
                , error: 'ValidationError'
                , validationErrors: err.errors
                })
            })
            .catch(ldaputil.InvalidCredentialsError, function(err) {
              log.warn('Authentication failure for "%s"', err.user)
              res.status(400)
                .json({
                  success: false
                , error: 'InvalidCredentialsError'
                })
            })
            .catch(function(err) {
              log.error('Unexpected error', err.stack)
              res.status(500)
              console.log("Este es el mock error 500" + err.stack)
                .json({
                  success: false
                , error: 'ServerError'
                })
            })
          break
        default:
          res.send(406)
          break
      }

      
    }
  
  })



  // app.post('/auth/api/v1/create', function(req, res){
  //   var user_name = req.body.user_name;
  //   var email = req.body.email;
  //   var correo = req.body.correo;
  //   var telefono = req.body.telefono;

  //   console.log(user_name)
  //  console.log(email)
  //   console.log(correo)
  //   console.log(telefono) 

     
  //               log.info('Authenticated "%s"', req.body.email)
  //               console.log("Entra al token")
  //               var token = jwtutil.encode({
  //                 payload: {
  //                   password: req.body.email
  //                 , user_name: req.body.user_name
  //                 , emailtrue: req.body.correo
  //                 , telefono: req.body.telefono
  //                 , email: req.body.email
  //                 , name: req.body.user_name
  //                 }
  //               , secret: options.secret
  //               , header: {
  //                   exp: Date.now() + 24 * 3600
  //                 }
  //               })
  //               res.status(200)
  //                 .json({
  //                   success: true
  //                 , redirect: urlutil.addParams(options.appUrl, {
  //                     jwt: token
  //                   })
  //                 })

  // })

  // app.post('/auth/api/v1/create', function(req, res) {
  //   var log = logger.createLogger('auth-create')
  //   log.setLocalIdentifier(req.ip)
  //   switch (req.accepts(['json'])){
  //     case 'json':
  //       requtil.validate(req, function() {
  //           req.checkBody('usuario').notEmpty()
  //           req.checkBody('email').notEmpty()
  //           req.checkBody('correo').notEmpty()
  //           req.checkBody('telefono').notEmpty()
  
  //         })
  //         .then(function() {
  //           log.info('Authenticated "%s"', req.body.email)
  //           var token = jwtutil.encode({
  //             payload: {
  //               email: req.body.email
  //             , name: req.body.name
  //             , correo: req.body.correo
  //             , telefono: req.body.telefono
  //             }
  //           , secret: options.secret
  //           , header: {
  //               exp: Date.now() + 24 * 3600
  //             }
  //           })
  //           res.status(200)
  //             .json({
  //               success: true
  //             , redirect: urlutil.addParams(options.appUrl, {
  //                 jwt: token
  //               })
  //             })
  //         })
  //         .catch(requtil.ValidationError, function(err) {
  //           console.log("Este es el requtil error 400" + err.stack)
  //           res.status(200)
  //             .json({
  //               success: true
  //             , redirect: urlutil.addParams(options.appUrl, {
  //                 jwt: token
  //               })
  //             })
  //         })

  //         .catch(ldaputil.InvalidCredentialsError, function(err) {
  //           console.log("Este es el ldap error 400" + err.stack)
  //           log.warn('Authentication failure for "%s"', err.user)
  //           res.status(400)
            
  //             .json({
  //               success: false
  //             , error: 'InvalidCredentialsError'
  //             })
  //         })
  //         .catch(function(err) {
  //           log.error('Unexpected error', err.stack)
  //           res.status(500)
  //             .json({
  //               success: false
  //             , error: 'ServerError'
  //             })
  //         })
  //       break
  //     default:
  //       res.send(406)
  //       break
  //   }
  // })

  server.listen(options.port)
  log.info('Listening on port %d', options.port)
}
