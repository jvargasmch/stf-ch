var cookieSession = require('cookie-session')
//var localStorage = require('localStorage')

module.exports = function(options) {
  var session = cookieSession(options)
  return function(socket, next) {
    var req = socket.request
    var res = Object.create(null)
    session(req, res, next)
  }
}


/*module.exports = function(options) {
  var session = localStorage(options)
  return function(socket, next) {
    var req = socket.request
    var res = Object.create(null)
    session(req, res, next)
  }
}*/
