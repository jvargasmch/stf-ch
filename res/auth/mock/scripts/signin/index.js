/*require('./signin.css')

module.exports = angular.module('stf.signin', ["ngRoute"])
  .config(function($routeProvider) {
    $routeProvider
      .when('/auth/mock/', {
        template: require('./signin.pug')
      })
      .when('/auth/mock/Olvido', {
        templateUrl: './Olvido.html',controller:'OlvidoCtrl'
      });
  })

  .controller('OlvidoCtrl', require('./Olvido-controller'))

  .controller('SignInCtrl', require('./signin-controller'))
  .controller('CreateCtrl', require('./create-controller'))
*/
  
  require('./signin.css')

  module.exports = angular.module('stf.signin', [])
    .config(function($routeProvider) {
      $routeProvider
        .when('/auth/mock/', {
          template: require('./signin.pug'),
          controller: 'SignInCtrl'
        })
        .when('/auth/register/', {
          template: require('./../olvido/olvido.pug'),
          controller: 'OlvidoCtrl'
        })
        .when('/auth/register/success/', {
          template: require('./../success/success.pug'),
          controller: 'SuccessCtrl'
        })
        .when('/auth/noregister/', {
          template: require('./../noregister/noregister.pug'),
          controller: 'NoregisterCtrl'
        })
        .otherwise({
          redirectTo: '/auth/mock'
        });
    })
    
    //.controller('SuccessCtrl', require('./success/success-controller'))
    .controller('OlvidoCtrl', require('./../olvido/olvido-controller'))
    // .controller('SuccessCtrl', require('./../success/success-controller'))
  
    .controller('SignInCtrl', require('./signin-controller'))
    .controller('CreateCtrl', require('./create-controller'))

// revisar el jwt del proyecto de adrian pulido.




