module.exports = function CreateCtrl($scope, $http) {

  this.$onInit = function (){

    console.log("create-controller hoy")
}

    $scope.error = null
  
    $scope.submit = function() {
        var data = {
            user_name: $scope.register.user_name.$modelValue
            , email: $scope.register.email.$modelValue
            , correo: $scope.register.correo.$modelValue
            , telefono:  $scope.register.telefono.$modelValue
          }
          $scope.invalid = false
          $http.post('/auth/api/v1/create', data)
            .success(function(response) {
              console.log("Entra a success function y se confirma el create-controller");
      
              $scope.error = null
              location.replace(response.redirect)
            })
            .error(function(response) {
              switch (response.error) {
                case 'ValidationError':
                console.log("Error");
                  $scope.error = {
                    $incorrect: true
                  }
                  break
                case 'InvalidCredentialsError':
                console.log("Error");
                  $scope.error = {
                    $incorrect: true
                  }
                  break
                default:
                console.log("Error default");
                  $scope.error = {
                    $server: true
                  }
                  break
              }
            })
    }
}